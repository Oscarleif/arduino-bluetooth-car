/*
 * CoordinatesHelperJNI.cpp
 *
 *  Created on: May 5, 2015
 *      Author: OscarLeif
 */

#include "CoordinatesHelperJNI.h"
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <jni.h>
#include <android/log.h>
#define CLASS_NAME "org/cocos2dx/cpp/AppActivity"
#include "JNIHelpers.h"
#endif

#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "PlayGameSingleton.h"
#endif



void CoordinatesHelperJNI::sendData(long x, long y)
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniHelpers::jniCommonVoidCall("showDataC", CLASS_NAME,(int) x, (int)y);
#endif
}
void CoordinatesHelperJNI::setSpeed(long speed)
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
	JniHelpers::jniCommonVoidCall("setSpeed", CLASS_NAME,(int) speed);
#endif
}
