#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    CC_SYNTHESIZE(Node*,_rootNode,RootNode);
    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
private:
    cocos2d::ui::Button *buttonStop;
    cocos2d::ui::Slider *sliderSpeed;
    cocos2d::ui::TextBMFont *speedLabel;
    void touchEventStop(Ref* pSender,ui::Widget::TouchEventType type);
    void sliderEvent(Ref *pSender, Slider::EventType type);
};

#endif // __HELLOWORLD_SCENE_H__
