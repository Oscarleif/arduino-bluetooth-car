/*
 * CoordinatesHelperJNI.h
 *
 *  Created on: May 5, 2015
 *      Author: OscarLeif
 */

#ifndef COORDINATESHELPERJNI_H_
#define COORDINATESHELPERJNI_H_

#include "cocos2d.h"
USING_NS_CC;

class CoordinatesHelperJNI {
public :
	static void sendData(long x, long y);
	static void setSpeed(long speed);
};

#endif /* COORDINATESHELPERJNI_H_ */
