/*
 * Constants.h
 *
 *  Created on: Sep 25, 2015
 *      Author: OscarLeif
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include "cocos2d.h"

#define WIN_SIZE Director::getInstance()->getWinSize()

#endif /* CONSTANTS_H_ */
