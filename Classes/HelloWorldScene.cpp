#include "HelloWorldScene.h"
#include "Joystick.h"
#include "CoordinatesHelperJNI.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    auto joystick = Joystick::create();
    joystick->setScale(0.80f);
    scene->addChild(joystick,5);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
    auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));

    // add the label as a child to this layer
    // this->addChild(label, 1);

    // add "HelloWorld" splash screen"
    //auto sprite = Sprite::create("HelloWorld.png");

    // position the sprite on the center of the screen
    //sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    // add the sprite as a child to this layer
    //this->addChild(sprite, 0);
    
    _rootNode = CSLoader::createNode("res/MainScene.csb");
    this->addChild(_rootNode,10);
    buttonStop =(cocos2d::ui::Button*) _rootNode->getChildByName("Button_1");
    buttonStop->addTouchEventListener(CC_CALLBACK_2(HelloWorld::touchEventStop,this));
    buttonStop->setVisible(false);
    buttonStop->removeFromParent();
    sliderSpeed = (cocos2d::ui::Slider*)_rootNode->getChildByName("Slider_1");
    sliderSpeed->addEventListener(CC_CALLBACK_2(HelloWorld::sliderEvent, this));
    speedLabel = (TextBMFont*)_rootNode->getChildByName("BitmapFontLabel_Speed");
    /** Just add cute animations **/
    Sprite* bgAnimate;
    SpriteFrameCache* spriteFrameCache = SpriteFrameCache::getInstance();
    //spriteFrameCache->addSpriteFramesWithFile("Plist.plist");
    Animation* anim = Animation::create();
    bgAnimate = Sprite::create();

   /* bgAnimate->setPosition(Vec2(WIN_SIZE.width * 0.5f, WIN_SIZE.height * 0.5f));
    for(int i=120;i<240;i++)
    {
    	__String* str = __String::createWithFormat("%04d.png",i);
    	log("sprite %s",str->getCString());
    	SpriteFrame *frame = spriteFrameCache->getSpriteFrameByName(str->_string);
    	anim->addSpriteFrame(frame);
    }
    anim->setDelayPerUnit(1.0f/20.0f);
    Animate *animateAction = Animate::create(anim);
    bgAnimate->runAction(RepeatForever::create(animateAction));
*/
    auto color_action = TintBy::create(0.5f, 0, 0, 10);
    auto color_back = color_action->reverse();
    auto seq = Sequence::create(color_action, color_back, nullptr);
    bgAnimate->runAction(RepeatForever::create(seq));
    Sprite* bg = Sprite::create("bg.png");
    bg->setPosition(Vec2(WIN_SIZE.width*0.5f,WIN_SIZE.height*0.5f));
    bg->setOpacity(150);
    this->addChild(bg,0);
    //this->addChild(bgAnimate,1);

    return true;
}
void HelloWorld::touchEventStop(Ref* pSender,ui::Widget::TouchEventType type)
{
	switch (type) {
		case Widget::TouchEventType::BEGAN:
			break;
		case Widget::TouchEventType::MOVED:
			CoordinatesHelperJNI::sendData(0, 0);
			break;
		case Widget::TouchEventType::ENDED: {

		}
			break;
		case Widget::TouchEventType::CANCELED:
			// log("Cancel the game start");
			break;
		default:
			break;
		}
}
void HelloWorld::sliderEvent(Ref *pSender, Slider::EventType type)
{
    if (type == Slider::EventType::ON_PERCENTAGE_CHANGED)
    {
        Slider* slider = dynamic_cast<Slider*>(pSender);
        int percent = slider->getPercent() ;
        int scalePercent = percent*(2.55f);
        //_displayValueLabel->setString(String::createWithFormat("Percent %d", percent)->getCString());
        CoordinatesHelperJNI::setSpeed((long)scalePercent);
        log("Velocity: %d	Percent: %d",scalePercent,percent);
        speedLabel->setString(__String::createWithFormat("Speed:%d",scalePercent)->getCString());
    }
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
