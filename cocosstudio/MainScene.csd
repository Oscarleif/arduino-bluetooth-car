<GameProjectFile>
  <PropertyGroup Type="Scene" Name="MainScene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="2.3.2.3" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="720.0000" Y="405.0000" />
        <Children>
          <AbstractNodeData Name="Button_1" Visible="False" ActionTag="282944544" Tag="7" IconVisible="False" LeftMargin="522.0666" RightMargin="151.9334" TopMargin="332.7408" BottomMargin="36.2592" TouchEnable="True" FontSize="14" ButtonText="Stop" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="46.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="545.0666" Y="54.2592" />
            <Scale ScaleX="2.1723" ScaleY="1.6877" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7570" Y="0.1340" />
            <PreSize X="0.0000" Y="0.0000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="255" G="127" B="80" />
          </AbstractNodeData>
          <AbstractNodeData Name="Slider_1" ActionTag="686894505" Tag="4" RotationSkewX="-90.0000" RotationSkewY="-90.0000" IconVisible="False" LeftMargin="520.5016" RightMargin="-0.5016" TopMargin="192.2645" BottomMargin="198.7355" TouchEnable="True" PercentInfo="50" ctype="SliderObjectData">
            <Size X="200.0000" Y="14.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="620.5016" Y="205.7355" />
            <Scale ScaleX="1.5286" ScaleY="2.1172" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8618" Y="0.5080" />
            <PreSize X="0.0000" Y="0.0000" />
            <BackGroundData Type="Default" Path="Default/Slider_Back.png" Plist="" />
            <ProgressBarData Type="Normal" Path="DnE15.png" Plist="" />
            <BallNormalData Type="Normal" Path="SliderNode_Normal.png" Plist="" />
            <BallPressedData Type="Normal" Path="SliderNode_Press.png" Plist="" />
            <BallDisabledData Type="Default" Path="Default/SliderNode_Disable.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_Title" ActionTag="497090781" Tag="5" RotationSkewX="10.0000" IconVisible="False" LeftMargin="241.6545" RightMargin="-75.6545" TopMargin="-21.0296" BottomMargin="338.0296" LabelText="Controls" ctype="TextBMFontObjectData">
            <Size X="554.0000" Y="88.0000" />
            <AnchorPoint ScaleY="0.4258" />
            <Position X="241.6545" Y="375.5000" />
            <Scale ScaleX="0.4290" ScaleY="0.4092" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3356" Y="0.9272" />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="BaseFont-export.fnt" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_Speed" ActionTag="187322274" Tag="6" IconVisible="False" LeftMargin="407.9666" RightMargin="-361.9666" TopMargin="158.5000" BottomMargin="158.5000" LabelText="Speed:120" ctype="TextBMFontObjectData">
            <Size X="674.0000" Y="88.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="407.9666" Y="202.5000" />
            <Scale ScaleX="0.2678" ScaleY="0.2678" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5666" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
            <LabelBMFontFile_CNB Type="Normal" Path="BaseFont-export.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameProjectFile>