#include <SoftwareSerial.h>
#include "bluetooth.h"

Bluetooth *blue = new Bluetooth("HC-05");

//Variables del motor Derecho
int ENA = 10;
int in1 = 11;
int in2 = 12;
//Varaibles del motor Izquierdo
int ENB = 9;
int in3 = 13;
int in4 = 8;
//
int velD = 255;
int velI= 255;

String msg;

void adelante()
{
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
}
void atras()
{
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
}
void detener()
{
  digitalWrite(in1,LOW);
  digitalWrite(in2,LOW);
  
  digitalWrite(in3,LOW);
  digitalWrite(in4,LOW);
  
  analogWrite(ENA,00);
  analogWrite(ENB,00);
}

void rotarDerecha()
{
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
}
void rotarIzquierda()
{
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
}
bool Contains(String s, String search)
{
  int max = s.length() - search.length(); // the searchstring has to fit in the other one  
  for (int i=0; i<= max; i++) 
  {
  if (s.substring(i) == search) return true;  // or i
  }
  return false;  //or -1
}
void setup() 
{
  Serial.begin(9600);
  blue->setupBluetooth(); 
  pinMode(7, OUTPUT);
  // put your setup code here, to run once:
  for(int x=8; x<=13 ;x++)
  {
    pinMode(x,OUTPUT);
  }
  analogWrite(ENA,velI);
  analogWrite(ENB,velD);  
}

void loop() {
  // put your main code here, to run repeatedly:
  msg = blue->Read();
  if(msg.length() > 1){
  //Serial.print("Received: ");
  //Serial.println(msg);


  if(msg.equals("0#"))
  {
    detener();
  }
  if(msg.equals("1#"))
  {
    Serial.println("Hacia el frente");
    //adelante();
    rotarDerecha();
  }
  if(msg.equals("2#"))
  {
    //atras();
    rotarIzquierda();
  }
  if(msg.equals("3#"))
  {
    //rotarDerecha();
    adelante();
  }
  if(msg.equals("4#"))
  {
   // rotarIzquierda();
   atras();
  }
  if(msg.indexOf("5#"))
  {
    msg = msg.substring(0,msg.length()-1);
    msg = msg.substring(0,msg.length()-1);
    
    analogWrite(ENA,msg.toInt());
    analogWrite(ENB,msg.toInt());  
  }
  
  if(Serial.available()){
  blue->Send("Example message#");
  }

}

}
