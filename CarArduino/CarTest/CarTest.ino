#include <SoftwareSerial.h>
#include "bluetooth.h"

Bluetooth *blue = new Bluetooth("HC-05");

//Varaibles del motor B
int ENA = 10;
int in1 = 11;
int in2 = 12;

//Varaibles del motor B
int ENB = 9;
int in3 = 13;
int in4 = 8;

int vel = 0;

void setup() 
{
  Serial.begin(9600);
  blue->setupBluetooth(); 
  pinMode(7, OUTPUT);
  // put your setup code here, to run once:
  for(int x=8; x<=13 ;x++)
  {
    pinMode(x,OUTPUT);
  }
  

}

void adelante()
{
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
}
void atras()
{
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
}

void giroDerecho()
{
  digitalWrite(in1,HIGH);
  digitalWrite(in2,LOW);
  
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
}

void giroIzquierdo()
{
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH) ;
  
  digitalWrite(in3,LOW);
  digitalWrite(in4,HIGH);
}

void detener()
{
  digitalWrite(in1,LOW);
  digitalWrite(in2,LOW);
  
  digitalWrite(in3,LOW);
  digitalWrite(in4,LOW);
}

void setCarSpeed(int speed)
{
  Serial.println(speed);
  analogWrite(ENA,250);
  analogWrite(ENB,250);
}

bool Contains(String s, String search) {
    int max = s.length() - search.length();

    for (int i = 0; i <= max; i++) {
        if (s.substring(i) == search) return true; // or i
    }

    return false; //or -1
} 

void loop() 
{  
  String msg = blue->Read();
  if(msg.length() > 1){
  Serial.print("Received: ");
  Serial.println(msg);
  
  digitalWrite(in1,LOW);
  digitalWrite(in2,HIGH);
  
  digitalWrite(in3,HIGH);
  digitalWrite(in4,LOW);
  
}
}


