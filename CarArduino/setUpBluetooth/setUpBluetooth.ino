#include <SoftwareSerial.h>


SoftwareSerial serial2(6,7);



void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  serial2.begin(38400);
  
  pinMode(8,OUTPUT);
  digitalWrite(8,HIGH);
  
  Serial.println("Inicio de comandos AT: ");
  
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available())
      serial2.write(Serial.read());
  
  if(serial2.available())
      Serial.write(serial2.read());
  
}
