/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.os.Bundle;
import android.util.Log;

public class AppActivity extends Cocos2dxActivity {
    
    static BluetoothArduino bluetooth = BluetoothArduino.getInstance("HC-05");
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
            // TODO Auto-generated method stub
            super.onCreate(savedInstanceState);
            setup();
    }
    public void setup() 
    {
            bluetooth.Connect();
            //textSize(30);
    }

    public static void showDataC(int x,int y)
    {
            //setup();
            //Toast.makeText(getContext(), "We recieve some data X: "+String.valueOf(x), (int) 0.2).show();
            Log.w("Arduino Controller Test", String.valueOf(x) + ", "+String.valueOf(y));
            sendMessagetoCar(y,x);
    }
    public static void sendMessagetoCar(int y, int x)
    {
            if(y == 0)
            {
                    bluetooth.SendMessage("0#");
                    
            }
            else if(y >= 250)
            {
                    bluetooth.SendMessage("1#");                    
            }               
            else if(y <=180)
            {
                    bluetooth.SendMessage("2#");
            }
            else if(x > 250)
            {
                    bluetooth.SendMessage("3#");
            }
            else if(x<190)
            {
                    bluetooth.SendMessage("4#");
            }
    }
    public static void setSpeed(int speed)
    {
            bluetooth.SendMessage(Integer.toString(speed)+"5#");
    }
    
    
}